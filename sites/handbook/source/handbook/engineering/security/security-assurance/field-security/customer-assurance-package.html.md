---
layout: handbook-page-toc
title: "GitLab's Customer Assurance Package"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab's Customer Assurance Package
At GitLab, we believe that [Transparency](https://about.gitlab.com/handbook/values/#transparency) is critical to our success- and security is no different. Our Customer Assurance Package (CAP) is designed to provide GitLab team members, users, customers, and other community members with the must current information about our Security and Compliance Posture. 

## Security Questionnaires and Assessments  
GitLab utilizes several mechanisms to aid our Customers and Prospects complete their Security Assessments. These resources capture the vast majority of questions and documentation normally requested as part of Security Assessments. They are accessible to anyone and a Non-Disclosure Agreement (NDA) is **not required** to view.

* [GitLab Security Trust Center](/security/)
* [Cloud Security Alliance (CSA)
Consensus Assessments Initiative Questionnaire (CAIQ)](https://cloudsecurityalliance.org/star/registry/gitlab/)
* [GitLab's Security Control Framework](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
* Information Security Policies and Procedures
    * [GitLab Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/)
    * [GitLab Password Policy](/handbook/security/#gitlab-password-policy-guidelines)
    * [GitLab Access Management Policy](/handbook/engineering/security/#access-management-process)
    * [GitLab Data Classification Policy](/handbook/engineering/security/data-classification-standard.html)
    * [GitLab Data Protection Impact Assessment Policy](/handbook/engineering/security/dpia-policy/)
    * [GitLab Penetration Testing Policy](/handbook/engineering/security/penetration-testing-policy.html)
    * [GitLab Audit Logging Policy](/handbook/engineering/security/audit-logging-policy.html)
     * [GitLab Security Incident Response Guide](/handbook/engineering/security/sec-incident-response.html)
     * [GitLab Business Continuity Plan](/handbook/business-ops/gitlab-business-continuity-plan.html)
* GitLab Architecture
    * [GitLab Application Architecture](https://docs.gitlab.com/ee/development/architecture.html)
    * [GitLab Production Architecture](/handbook/engineering/infrastructure/production/architecture/)
    * [High-level Network Diagram](/handbook/engineering/infrastructure/production/architecture/#network-architecture)
* GitLab Blog Post: [Securing your Instance Best Practices](/blog/2020/05/20/gitlab-instance-security-best-practices/)

Coming soon: Regulated Markets Customer Assurance Packages!

## Evidence Requests
In line with specific laws, regulations and contractual requirements, there are certain items that we require a **Non-Disclosure Agreement** to provide.  

* SOC2 Type 1 Report 
    * You can request a copy of the GitLab SOC2 Type 1 report by following [these instructions.](/handbook/engineering/security/security-assurance/security-compliance/soc2.html#requesting-a-copy-of-the-gitlab-soc2-type-1-report)
* GitLab Annual Third Party Penetration Test Results 
    * You can request a **Detailed Letter of Engagement** that includes a summary of the testing performed and high level results by following [these instructions](/security/#external-testing).

## Questions?
* If you have any further questions that aren't answered here please:
    * If you are not a GitLab team-member, contact security@gitlab.com.
    * If you are a GitLab team-member, reach out to Field Security via slack [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70), or [open an issue](https://gitlab.com/gitlab-com/gl-security/field-security/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) on our issue board.

    
