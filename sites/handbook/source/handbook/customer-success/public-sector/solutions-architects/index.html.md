---
layout: handbook-page-toc
title: Public Sector Solutions Architects
---
# Public Sector Solutions Architect Handbook
{:.no_toc}

Solutions Architects (SA) who work with the [Public Sector](/handbook/sales/public-sector/) provide subject matter expertise and industry experience throughout the sales process to Public Sector customers within the United States.

Because specific requirements and common engagement practices differ from Enterprise or Commercial customers, the guidance below exists to assist Solutions Architects who work with customers in the Public Sector specifically.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Federal Support

The GitLab Support Team provides [U.S.-based support](https://about.gitlab.com/support/#us-federal-support) for those customers that require U.S. citizens to work their support tickets.

- The user that submits the ticket needs to be associated with the appropriate account in SFDC.
- All accounts and user information get synched between SFDC and Zendesk on an hourly basis under the conditions detailed on the [Support Ops page](/handbook/support/support-ops/responsibilities.html#sfdcus-federal-zendesk-sync).
- All communications with support will be asynchronous, unless a synchronous call is requested by the Technical Account Manager and/or Solutions Architect for that account.
- Only customers designated as Public Sector in SFDC are eligible for Federal Support.

## Technical Close Plans

Technical Close Plans provide insight and transparency to the sales process by highlighting the technical ecosystem, competitive landscape, evaluation goals and technical steps required in order to achieve a technical win (Salesforce Stage 3). These plans are templated and only required for opportunities exceeding $100K in revenue.

To create a technical close plan:

- Log in to Salesforce and locate the relevant Opportunity
- Ensure the Opportunity is at stage 3 or is soon transitioning to stage 3
- Make a copy of the template document located in the Public Sector Google drive and rename it based on the customer and opportunity
- Complete the document fields with all known information
- Update the technical close plan with outcomes and next steps after each customer interaction

When the opportunity progresses to stage 4, the technical close plan is complete. A brief retrospective on the information helps the team identify trends in customer needs as well as clear paths to opportunity wins. In case of opportunity loss, a brief retrospective on the information can help populate the Closed Lost reason in Salesforce.


