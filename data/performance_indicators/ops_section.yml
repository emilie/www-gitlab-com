- name: Ops - Section TMAU - Sum of Ops Section SMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A sum of all SMAUs across the five stages in the Ops Section (Verify,
    Package, Release, Configure & Monitor)
  target: 466k by End of Q3
  org: Ops Section
  public: true
  telemetry_type: Both
  pi_type: Section TMAU
  plan_type: Both
  pi_workflow: Complete
  is_primary: true
  is_key: false
  sisense_data:
    chart: 9082293
    dashboard: 634200
    embed: v2
  health:
    level: 3
    reasons:
    - Goal Attainment - Surpassed with below caveat
    - Insight - High growth in Verify and Release (18% and 19% respectively)
    - Improvement - [CI Adoption Journey Research](https://gitlab.com/groups/gitlab-org/-/epics/4124)
    - Insight - Investigation required for [sudden spike in Monitor Proxy SMAU](https://gitlab.com/gitlab-com/Product/-/issues/1520).
  instrumentation:
    level: 2
    reasons:
    - Now have a planned [Package SMAU](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/index.html#packagepackage---smau---count-of-users-publishinginstalling-packages)
    - Looking to improve our proxy Monitor and Configure SMAUs inluding [adjusting Configure SMAU to be based on Kubernetes and IaC usage](https://gitlab.com/gitlab-org/growth/product/-/issues/1612)
  urls:
  - https://about.gitlab.com/direction/ops/#performance-indicators
  - https://about.gitlab.com/direction/ops/#user-adoption-journey

- name: Verify, Verify:CI, Verify:Pipeline Authoring, Verify:Runner - SMAU, GMAU - Unique users triggering pipelines
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of unique users triggering pipelines in the last 28
    days
  target: Our FY21 Q3 Target is to see 10% growth. We are tracking towards a goal
    of 500k unique users triggering pipelines by end of FY21 (Jan 31, 2021).
  org: Ops Section
  stage: verify
  group: continuous_integration, pipeline_authoring, runner
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: SMAU, GMAU
  pi_workflow: Complete
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/verify/continuous-integration/#usage-funnel
  - https://about.gitlab.com/handbook/engineering/development/ops/verify/runner/#usage-funnel
  health:
    level: 3
    reasons:
    - Rationale - CI is the foundation for several downstream product areas, and improving
      SMAU for people using pipelines is important to help with discovery of these further
      features. Unique users who start pipelines are clearly engaging with CI, and so this
      is a clean measure that we can start with to track this.
    - Improvement - One area of improvement we want to invest in is the authoring
      experience, particularly around getting to your first successful pipeline quickly.
      This will help more teams adopt GitLab CI, improving SMAU. An example of a feature
      that could improve this capability is the ability to find and utilize [organization-wide recipes](https://gitlab.com/gitlab-org/gitlab/-/issues/24939)
      to create your CI pipelines.
    - Improvement - Another improvement in the Runner area is to [Provide automated setup and configuration](https://gitlab.com/groups/gitlab-org/-/epics/2778)
      of Runners for self-managed, and [enterprise management](https://gitlab.com/groups/gitlab-org/-/epics/4015)
      capabilities for Runners at scale. This drives adoption by making it easier for teams
      to get up and running quickly.
    - Improvement - Users engaging with pipelines but not necessarily triggering them is
      also important, but we are not yet considering that.
  instrumentation:
    level: 3
    reasons:
    - With ability to trigger a pipeline dependent on successful attempts to author
      CI yaml files, we have an issue to [track number of new .gitlab-ci.yml files
      created](https://gitlab.com/gitlab-org/gitlab/-/issues/232814).
    - We are also working to [instrument other performance indicators](https://gitlab.com/gitlab-data/analytics/-/issues/5319)
      for Verify:CI
    - We are also adding [instrumentation for time to first green build](https://gitlab.com/gitlab-org/gitlab/-/issues/232814) to measure Verify:Pipeline Authoring efficiency.
  sisense_data:
    chart: 9090628
    dashboard: 634200
    embed: v2

- name: Verify:Testing - Paid GMAU - Count of active paid testing feature users
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of unique users who have interacted with a paid testing
    feature in the last 28 days
  target: TBD
  org: Ops Section
  stage: verify
  group: testing
  public: true
  telemetry_type: SaaS
  plan_type: Paid
  pi_type: Paid GMAU
  pi_workflow: Instrument Tracking
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/verify/testing/#performance-indicators
  health:
    level: 0
    reasons:
    - Insights - We enabled tracking for one feature of Visual Reviews and have found very low usage especially compared to views of the docs page.
    - Improvements - We will be recording a walk through of how to setup and use Visual Reviews to add to the documentation page before the next review.
    - Improvements - We are targeting delivery of [Group Code Coverage](https://gitlab.com/groups/gitlab-org/-/epics/2991) features and tracking in 13.4.
  instrumentation:
    level: 2
    reasons:
    - Improvements - Implementation of page views for existing paid features [is in
      progress](https://gitlab.com/groups/gitlab-org/-/epics/2991).
    - Improvements - We are targeting tracking of [Web Performance engagement](https://gitlab.com/gitlab-org/gitlab/-/issues/224603) in 13.5.
  sisense_data:
    chart: 9397356
    dashboard: 633395
    embed: v2
- name: Package:Package - Other - Packages published/installed using the Package Registry
  base_path: "/handbook/product/performance-indicators/"
  definition: A count of events in which a package is published to or installed from
    the Package Registry on GitLab.com.
  target: The original FY21 Q3 target of 55k events per day, a 37% increase from Q2, has already been reached. 70k events per day is our new target.
  org: Ops Section
  stage: package
  group: package
  public: true
  telemetry_type: SaaS
  plan_type: None
  pi_type: Other
  pi_workflow: Complete
  is_primary: false
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/package/#usage-funnels
  health:
    level: 3
    reasons:
    - Insight - PyPI adoption is growing faster in adoption than Composer.
    - Insight - Due to increased adoption of npm, NuGet and PyPI, we have already hit our Q3 goal of 55k events per day.
    - Insight - We've seen a 50% increase in page views to the Package Registry, likely the result of UX improvements.
    - Improvements - In 13.4, we've made several improvements to the Conan Repository. We'd like to see a 10% increase in adoption by the end of 13.5.
    - Improvements - Milestone 13.5 we will drive usage by resolving bugs with the NuGet, PyPI and npm registries. We'll also continue to iterate on and improve the Package Registry UI.
  instrumentation:
    level: 3
    reasons:
    - We are currently tracking this data for GitLab.com via Snowplow.
    - Here is the [issue defining the PI](https://gitlab.com/gitlab-data/analytics/-/issues/4597)
  sisense_data:
    chart: 8104230
    dashboard: 527857
    embed: v2
- name: Package, Package:Package - SMAU, GMAU - Count of users publishing/installing packages
  base_path: "/handbook/product/performance-indicators/"
  definition: The maximum distinct count of users who either published a package or
    interacted with an installed package from the Package Registry. In the future,
    this value will the distinct count of users across all Package AMAUs.
  target: n/a
  org: Ops Section
  stage: package
  group: package
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: SMAU, GMAU
  pi_workflow: Instrument Tracking
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/package/#aarrr-framework
  health:
    level: 0
    reasons:
    - Improvements - In 13.4, we intend to start measuring GMAU/SMAU for the Package
      stage via usage ping.
  instrumentation:
    level: 1
    reasons:
    - We have an issue open to [measure this data via the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/205578#note_368506625). This work is on-track to be completed in 13.4.
- name: Package:Package - Paid GMAU - Count of paid users using the Dependency Proxy
  base_path: "/handbook/product/performance-indicators/"
  definition: The maximum distinct count of <a href="https://about.gitlab.com/handbook/product/performance-indicators/#paid-user">paid users</a> who either published a package or interacted with an installed package from the Package Registry. In the future, this value will the distinct count of users across all Package AMAUs.
  target: n/a
  org: Ops Section
  stage: package
  group: package
  public: true
  telemetry_type: Both
  plan_type: Paid
  pi_type: Paid GMAU
  pi_workflow: Instrument Tracking
  is_primary: false
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/package/#usage-funnels
  health:
    level: 0
    reasons:
    - Not yet instrumented
  instrumentation:
    level: 1
    reasons:
    - We have an issue open to [measure Dependency Proxy adoption via the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/238056). It is currently scheduled for milestone 13.6.
- name: Package:Package - AMAU - Count of users publishing packages
  base_path: "/handbook/product/performance-indicators/"
  definition: The distinct count of users who publish a package to the Package Registry.
  target: n/a
  org: Ops Section
  stage: package
  group: package
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Instrument Tracking
  is_primary: false
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/package/#usage-funnels
  health:
    level: 0
    reasons:
    - Not yet instrumented
  instrumentation:
    level: 1
    reasons:
    - In 13.4, we will begin to [measure user level adoption of the Package Registry with the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/205578). This work is on-track to be completed in 13.4.
- name: Package:Package - AMAU - Count of users installing packages
  base_path: "/handbook/product/performance-indicators/"
  definition: The distinct count of users who interact with a package that is installed
    from the Package Registry.
  target: n/a
  org: Ops Section
  stage: package
  group: package
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Instrument Tracking
  is_primary: false
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/package/#usage-funnels
  health:
    level: 0
    reasons:
    - Not yet instrumented
  instrumentation:
    level: 1
    reasons:
    - In 13.4, we will begin to [measure user level adoption of the Package Registry with the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/205578). This work is on-track to be completed in 13.4.
- name: Release, Release:Progressive Delivery, Release:Release Management - SMAU, GMAU - Count of users triggering deployments using GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of unique users triggering deployments with Gitlab in
    the last 28 days.
  target: Our FY21 Q3 Target is to see a growth of 24%.
  org: Ops Section
  stage: release
  group: progressive_delivery, release_management
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: SMAU, GMAU
  pi_workflow: Complete
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Insight -  In August we surpassed 100K unique users triggering deployments, nearly doubling the percent growth from the prior month.
      FY21 Q2 at 21%. We hope to hit this target again and slightly increase it in FY21 Q3.
    - Improvements - We are seeing a very steep increase since June for self-managed users - nearly 15,000 users, with SaaS users trending as expected with ~2K users. We see that the greatest percentage increase came from Silver and Gold accounts.
    - Improvements - By delivering more at scale features in Release Management, to
      include sharing [releases](https://gitlab.com/groups/gitlab-org/-/epics/3561)
      and [environments](https://gitlab.com/gitlab-org/gitlab/-/issues/196168) at
      the group level we expect to positively influence the reach to users planning
      releases and deploying to many environments. On the Progressive Delivery front,
      continued investment in making it easy to [deploy to AWS](https://gitlab.com/gitlab-org/gitlab/-/issues/196049)
      will reduce friction to deploying to AWS as target.
  instrumentation:
    level: 3
    reasons:
    - We are currently tracking this data for GitLab.com via Snowplow and as a usage
      ping for self-managed instances.
    - We need to [Expand Revenue Attribution to Release SMAU](https://gitlab.com/gitlab-data/analytics/-/issues/4461)
    - Improvements - Counter was instrumented for 28-days
    - Improvements - User and unique count of deployments are instrumented
    - Improvements - SaaS and self-managed data are both available on a single chart
  sisense_data:
    chart: 8578386
    dashboard: 661492
    embed: v2
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/release/#release-smau--release-management--progressive-delivery-gmau-usage-funnel
  - https://app.periscopedata.com/app/gitlab/640196/New-WIP-GitLab.com-+-Self-Managed-Usage-Ping-SMAU
  - https://app.periscopedata.com/app/gitlab/661492/Temp---Eli-Release-Stage-Examples?widget=8578558&udv=0

- name: Release:Release Management - AMAU - Unique user count viewing Release and
    Environment pages
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of environment page and release page view for Gitlab.com
    in the last 28 days.
  target: Our FY21 Q3 Target is to see a growth of 10%.
  org: Ops Section
  stage: release
  group: release_management
  public: true
  telemetry_type: SaaS
  plan_type: None
  pi_type: AMAU
  pi_workflow: Complete
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - Insights - We have seen an increase in Releases created since August, which I would attribute to the introduction of Release Generation from YAML.
    - Improvements - We are expecting to deliver Assets in Releases in 13.5 which will complete the Release Orchestration from the Releases Page in addition to supporting Group Releases in 13.7. Both of these features should positively impact the environment page views and Releases created.
    - Improvements - Counter was instrumented for 28-days
    - Improvements - Unique counts of views for both Release Page and Environments
      Page
    - Improvements - SaaS and self-managed data are both available on a single chart
  instrumentation:
    level: 2
    reasons:
    - We are currently tracking this data for GitLab.com via Snowplow
  sisense_data:
    chart: 7001781
    dashboard: 540095
    embed: v2

- name: Release:Progressive Delivery:Feature Flags - AMAU - Unique users triggering
    feature flag toggles
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of users triggering feature flag toggles with Gitlab
    in the last 28 days.
  target: TBD
  org: Ops Section
  stage: release
  group: progressive_delivery
  category: feature_flags
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Instrument Tracking
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - Not yet instrumented
  instrumentation:
    level: 1
    reasons:
    - Improvements - Create the instrumentation [Count UI Feature Flag Toggles - Snowplow](https://gitlab.com/gitlab-org/gitlab/-/issues/31970) in progress in 13.4 
    - Improvements - Create the instrumentation [Count API Feature Flag Toggles -
      Snowplow](https://gitlab.com/gitlab-org/gitlab/-/issues/233872)
    - Improvements - Add Usage Ping data

- name: Release:Progressive Delivery:Continuous Delivery - AMAU - Unique users triggering
    pipelines with automatic deployments (no manual jobs)
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of users triggering automatic deployments (in without
    any manual activity) with Gitlab in the last 28 days.
  target: TBD
  org: Ops Section
  stage: release
  group: progressive_delivery
  category: continuous_delivery
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Instrument Tracking
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - Not yet instrumented
  instrumentation:
    level: 0
    reasons:
    - Improvements - Create the instrumentation [Instrument AMAU of CD](https://gitlab.com/gitlab-org/gitlab/-/issues/233928)

- name: Release:Progressive Delivery:Review Apps - AMAU - Unique users triggering
    pipelines that created a review app
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of unique users triggering pipelines which created a
    review app with Gitlab in the last 28 days.
  target: TBD
  org: Ops Section
  stage: release
  group: progressive_delivery
  category: review_apps
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Instrument Tracking
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No yet instrumented
  instrumentation:
    level: 0
    reasons:
    - Improvements - Revive the instrumentation

- name: Release:Progressive Delivery:Advanced Deployments - AMAU - Count of unique
    users using Canary or Incremental Rollout
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of users using advanced deployments, namely, canary
    and incremental rollouts with Gitlab in the last 28 days.
  target: TBD
  org: Ops Section
  stage: release
  group: progressive_delivery
  category: advanced_deployments
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Instrument Tracking
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No yet instrumented
  instrumentation:
    level: 1
    reasons:
    - Improvements - Create the instrumentation [Instrument AMAU of Canary Rollouts](https://gitlab.com/gitlab-org/gitlab/-/issues/233899)
    - Improvements - Create the instrumentation [Instrument AMAU of Incremental Rollouts](https://gitlab.com/gitlab-org/gitlab/-/issues/233903)

- name: Release:Progressive Delivery:Deployment to AWS - AMAU - Count of users using
    AutoDevOps or Ci templates to deploy to AWS
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of users using using AutoDevOps or Ci templates to deploy
    to AWS based on  cloud_service:$CI_CLUSTER_CLOUD_SERVICE_TYPE in the last 28 days.
  target: TBD
  org: Ops Section
  stage: release
  group: progressive_delivery
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Instrument Tracking
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - Not yet instrumented
  instrumentation:
    level: 1
    reasons:
    - Improvements - Create the instrumentation [Show the metric "The projects count per platform target" on Periscope dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/217707) and by the Telemtry group in [Deployment Target Data Request](https://gitlab.com/gitlab-data/analytics/-/issues/5938) in 13.4

- name: Configure, Configure:Configure - SMAU, GMAU - Proxy user count based on attached clusters
  base_path: "/handbook/product/performance-indicators/"
  definition: A count of all users in projects that have an attached cluster because
    project users receive benefit from the cluster without direct usage.
  target: 5k Configure SMAU by the end of Q3FY21 (20% growth)
  org: Ops Section
  stage: configure
  group: configure
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: SMAU, GMAU
  pi_workflow: Complete
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Insight - 17% increase from Jul/Aug puts the metric on target for hitting Q3 goal.
  instrumentation:
    level: 2
    reasons:
    - Proxy SMAU that we are looking to [update to be based on the below IaC and Kubernetes Management AMAUs](https://gitlab.com/gitlab-org/growth/product/-/issues/1612)
    - Insight - With our IaC developments, we've started to collect reliable, usage-oriented metrics
    - Improvement - we want to start collecting activity-based data with the GitLab Kubernetes Agent to be released soon
  sisense_data:
    chart: 9090632
    dashboard: 634200
    embed: v2

- name: Configure:Configure - AMAU - Number of projects with GitLab Managed Terraform State
  base_path: "/handbook/product/performance-indicators/ops-section-performance-indicators/"
  definition: A rolling count of the number of projects using GitLab Managed Terraform State in the last 28 days
  target: 700 by end of Q3FY21
  org: Ops Section
  stage: configure
  group: configure
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Complete
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
      - Insight - This the first metric on our Infrastructure as Code features
      - Insight - We see a clear interest among Ultimate users
      - Insight - The numbers are just coming in given the self-managed upgrade cycles
  instrumentation:
    level: 2
    reasons:
      - We are still missing the namespace/tier based visualisations for SasS, currently SaaS is counted as an Ultimate user
  sisense_data:
    chart: 9080255
    dashboard: 677628
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/677628/Infrastructure-as-Code

- name: Configure:Configure - AMAU - Number of Gitlab Kubernetes Agent instances
  base_path: "/handbook/product/performance-indicators/ops-section-performance-indicators/"
  definition: A rolling count of the number of GitLab Kubernetes Agent instances in the last 28 days
  target: 100
  org: Ops Section
  stage: configure
  group: configure
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: AMAU
  pi_workflow: Data Availability
  is_primary: false
  is_key: false
  health:
    level: 1
    reasons:
      - No data received yet
  instrumentation:
    level: 3
    reasons:
      - We intend to ship the GitLab Kubernetes Agent on the 22nd with instrumentation included
      - We developed proper instumentation by the launch of the agent
      - The instrumentation was already added to usage ping
  sisense_data:
    chart: 9639658
    dashboard: 656814
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration

- name: Configure:Configure - Other - Number of CI/CD pipeline runs going through
    our infrastructure management deployment oriented features
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of pipelines calling infrastructure management deployment
    commands in the last 28 days
  target: TBD
  org: Ops Section
  stage: configure
  group: configure
  public: true
  telemetry_type: SaaS
  plan_type: None
  pi_type: Other
  pi_workflow: PI Target
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
    - Auto DevOps - need to figure out [a way to measure templates usage](https://gitlab.com/gitlab-org/gitlab/-/issues/219457)
    - Insight - IaC - adoption showed up really quickly. 2.7% and 2.5% of premium
      and gold namespaces started to use Terraform features on gitlab.com. We see
      active usage of terraform reports generated (8000+ by silver users). bronze
      and free users use our features much less.
    - Insight - Kubernetes - had a design sprint, working on fixing the biggest shortcomings
      to increase adoption, currently [working to ship new approach](https://gitlab.com/gitlab-org/gitlab/-/issues/220868)
  instrumentation:
    level: 2
    reasons:
    - We're still missing reliable metrics for self-managed instances.
    - Improving chart to [single metric across all infrastructure deployment types](https://gitlab.com/gitlab-data/analytics/-/issues/4638)
  sisense_data:
    chart: 7833255
    dashboard: 511813
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/511813/Configure-team-business-metrics

- name: Monitor - SMAU - Proxy user count based on prometheus integration
  base_path: "/handbook/product/performance-indicators/"
  definition: A count of all users in projects that have a connected Prometheus integration
    because project users receive benefit from the cluster without direct usage.
  target: 11k Monitor SMAU by the end of Q3FY21 (20% growth)
  org: Ops Section
  stage: monitor
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: SMAU
  pi_workflow: Complete
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Insight - Far surpassed goal
    - Insight - We are investigating the [sudden spike in this metric](https://gitlab.com/gitlab-com/Product/-/issues/1520)
  instrumentation:
    level: 2
    reasons:
    - Utilizes proxy SMAU that we need to replace
  sisense_data:
    chart: 9090633
    dashboard: 634200
    embed: v2

- name: Monitor:Health - GMAU - Unique users that interact with Alerts and Incidents
  base_path: "/handbook/product/performance-indicators/"
  definition: Unique users that interacted with alerts or incidents
  target: TBD once we understand current activity
  org: Ops Section
  stage: monitor
  group: health
  public: true
  telemetry_type: Both
  plan_type: Both
  pi_type: GMAU
  pi_workflow: Instrument Tracking
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - Improvements - Health Group is focused on [table-stakes](https://brandmarketingblog.com/articles/branding-definitions/table-stakes-business/)
      features to build user base. In the next few milestones we are going to finish building out [Incidents as a type of issue](https://gitlab.com/groups/gitlab-org/-/epics/3853) and move on to enabling users to integrate any tool with GitLab via [custom integrations](https://gitlab.com/groups/gitlab-org/-/epics/4054). Making it easier to integrate alert sources into GitLab will increase adoption of all Incident Management features.
  instrumentation:
    level: 1
    reasons:
    - Improvements - Instrument GMAU as an aggregate counter in the usage ping for significant AMAUs - progress tracked in this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/233933).
  urls:
  - https://app.periscopedata.com/app/gitlab/668012/Health-Group-Dashboard
  - https://about.gitlab.com/handbook/engineering/development/ops/monitor/health/#north-star-metric
- name: Monitor - AMAU - Count of users creating incidents
  base_path: "/handbook/product/performance-indicators/"
  definition: Unique users who manually create incidents
  target: TBD
  org: Ops Section
  stage: monitor
  public: true
  telemetry_type: SaaS
  plan_type: None
  pi_type: AMAU
  pi_workflow: Complete
  is_primary: false
  is_key: false
  health:
    level: 0
    reasons:
    - This is an interim performance indicator while we work to instrument the GMAU for Monitor.
    - Health level is 0 because we do not yet understand a reasonable threshold for this metric
    - Insights - we are seeing month over month growth in the number of unique users creating incidents - this indicates increase in general awareness in the community
    - Improvements - In 13.4 and 13.5 we will finish building out [Incidents as a type of issue](https://gitlab.com/groups/gitlab-org/-/epics/3853)
  instrumentation:
    level: 2
    reasons:
    - In 13.4, we are adding instrumentation to collect usage ping data - work tracked on this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/233933)
  sisense_data:
    chart: 9635918
    dashboard: 737489
    embed: v2

- name: Monitor:Health - Other - Count of projects where incidents are being created
  base_path: "/handbook/product/performance-indicators/"
  definition: Count of projects where incidents are being created
  target: 100 projects where incidents are being created for GitLab.com by the end
    of Q3 - October 2020
  org: Ops Section
  stage: monitor
  group: health
  public: true
  telemetry_type: SaaS
  plan_type: None
  pi_type: Other
  pi_workflow: Complete
  is_primary: false
  is_key: false
  health:
    level: 1
    reasons:
    - Insights - We are below our current target because the incident management product
      is new and this is the first time we've set a target.
    - Insights - We are seeing a month over month increase from December 2019 to present. We can surmise this is because we've enabled
      the end-to-end triage workflow fully in GitLab during that time.
    - Improvements - Health Group is focused on [table-stakes](https://brandmarketingblog.com/articles/branding-definitions/table-stakes-business/)
      features to build user base. In the next few milestones we are going to finish building out [Incidents as a type of issue](https://gitlab.com/groups/gitlab-org/-/epics/3853) and move on to enabling users to integrate any tool with GitLab via [custom integrations](https://gitlab.com/groups/gitlab-org/-/epics/4054). Making it easier to integrate alert sources into GitLab will increase adoption of all Incident Management features.
  instrumentation:
    level: 2
    reasons:
    - Data is currently being captured for GitLab.com
    - Improvements - We need to instrument and dashboard data for the usage ping -
      work for this is being tracked [here](https://gitlab.com/gitlab-org/gitlab/-/issues/233719).
    - Improvements - We recently created incidents as a type of issue in GitLab. This means that we no longer need to use the incident label to indicate an issue is an incident. We are working to add a data set to this chart that tracks the creation of incidents over time in comparison to those using issues labeled with incident.
  sisense_data:
    chart: 9635914
    dashboard: 737489
    embed: v2

- name: Monitor:APM (Legacy) - Other - Total unique users that view metrics or logs
  base_path: "/handbook/product/performance-indicators/"
  definition: Total number of unique users that view metrics and log on GitLab.com
  target:
  org: Ops Section
  stage: monitor
  public: true
  telemetry_type: SaaS
  plan_type: None
  pi_type: Other
  pi_workflow: PI Target
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
      - Legacy
  instrumentation:
    level: 2
    reasons:
      - Legacy
  sisense_data:
    chart: 9033545
    dashboard: 636549
    embed: v2
  url:
  - https://app.periscopedata.com/app/gitlab/636549/APM---CUSTOMER-USAGE-(Logs-+-Metrics)
  - https://about.gitlab.com/handbook/engineering/development/ops/monitor/apm/#north-star-metric-nsm
